import { check } from 'meteor/check';
import { TasksCollection } from '../db/TasksCollection';
import { Accounts } from 'meteor/accounts-base';
 
Meteor.methods({
	'AddTaskData'(text) {
		check(text, String);
	
		if (!this.userId) {
			throw new Meteor.Error('Not authorized.');
		}
	
		TasksCollection.insert({
			text,
			createdAt: new Date,
			isChecked: false,
			userId: this.userId,
		})
	},
	
	'tasks.remove'(taskId) {
		check(taskId, String);
	
		if (!this.userId) {
			throw new Meteor.Error('Not authorized.');
		}

		const task = TasksCollection.findOne({ _id: taskId, userId: this.userId });

		if (!task) {
		  throw new Meteor.Error('Access denied.');
		}
	
		TasksCollection.remove(taskId);
	},
	
	'tasks.setIsChecked'(taskId, isChecked) {
		check(taskId, String);
		check(isChecked, Boolean);
	
		if (!this.userId) {
			throw new Meteor.Error('Not authorized.');
		}

		const task = TasksCollection.findOne({ _id: taskId, userId: this.userId });

		if (!task) {
		  throw new Meteor.Error('Access denied.');
		}
	
		TasksCollection.update(taskId, {
			$set: {
				isChecked
			}
		});
	},

	'registerUser'(username, password)
	{
		if (!Accounts.findUserByUsername(username)) {
			Accounts.createUser({
			  username: username,
			  password: password,
			});
		}

	}
});