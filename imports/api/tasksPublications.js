import { Meteor } from 'meteor/meteor';
import { TasksCollection } from '/imports/db/TasksCollection';

Meteor.publish('getListTasksByUser', function publishTasks() {
  return TasksCollection.find({ userId: this.userId });
});