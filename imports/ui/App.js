import { Template } from 'meteor/templating';
import { TasksCollection } from "../db/TasksCollection";
import { ReactiveDict } from 'meteor/reactive-dict';

import './App.html';
import './Task.js';
import "./Login.js";

const getUser = () => Meteor.user();
const isUserLogged = () => !!getUser();

const getTasksFilter = () => {
	const user = getUser();
  
	const hideCompletedFilter = { isChecked: { $ne: true } };
  
	const userFilter = user ? { userId: user._id } : {};
  
	const pendingOnlyFilter = { ...hideCompletedFilter, ...userFilter };
  
	return { userFilter, pendingOnlyFilter };
}

const HIDE_COMPLETED_STRING = "hideCompleted";
const IS_LOADING_STRING = "isLoading";

Template.mainContainer.onCreated(function mainContainerOnCreated() {
	this.state = new ReactiveDict();

	const handler = Meteor.subscribe('getListTasksByUser');
	Tracker.autorun(() => {
	  this.state.set(IS_LOADING_STRING, !handler.ready());
	});
});
 
Template.mainContainer.helpers({
	tasks() {
		const instance = Template.instance();
		const hideCompleted = instance.state.get(HIDE_COMPLETED_STRING);

		const { pendingOnlyFilter, userFilter } = getTasksFilter();

		if (!isUserLogged()) {
			return [];
		}

		return TasksCollection.find(hideCompleted ? pendingOnlyFilter : userFilter, {
			sort: { createdAt: -1 },
		}).fetch();
	},
	hideCompleted() {
		return Template.instance().state.get(HIDE_COMPLETED_STRING);
	},
	incompleteCount() {
		const incompleteTasksCount = TasksCollection.find({ isChecked: { $ne: true } }).count();
		if(incompleteTasksCount) {
			return `(${incompleteTasksCount})`;
		} else {
			return '';
		}
	},
	completeCount() {
		const completeTasksCount = TasksCollection.find({ isChecked: true }).count();
		if(completeTasksCount) {
			return `(${completeTasksCount})`;
		} else {
			return '';
		}
	},
	totalCount(){
		var totalAll = TasksCollection.find({ }).count();
		return `(${totalAll})`;
	},
	isUserLogged() {
		return isUserLogged();
	},
	getUser() {
		return getUser();
	},
	isLoading() {
		const instance = Template.instance();
		return instance.state.get(IS_LOADING_STRING);
	}
	
});


Template.mainContainer.events({
	"click #hide-completed-button"(event, instance) {
	  const currentHideCompleted = instance.state.get(HIDE_COMPLETED_STRING);
	  instance.state.set(HIDE_COMPLETED_STRING, !currentHideCompleted);
	},
	'click .user'() {
		Meteor.logout();
	}
});

Template.form.events({
	"submit .task-form"(event) {
		// Prevent default browser form submit
		event.preventDefault();
	
		// Get value from form element
		const target = event.target;
		const text = target.text.value;
	
		// Insert a task into the collection
		// TasksCollection.insert({text,
		// 						userId: getUser()._id,
		// 						isChecked: false, 
		// 						createdAt: new Date() 
		// 					});

		Meteor.call('AddTaskData', text);
	
		// Clear form
		target.text.value = '';
	},
})