import { Template } from 'meteor/templating';

import { TasksCollection } from "../db/TasksCollection";

import './Task.html';

Template.task.events({
    'click .toggle-checked'() {
        // Set the checked property to the opposite of its current value
        // TasksCollection.update(this._id, {
        //     $set: { isChecked: !this.isChecked },
        // });
        Meteor.call('tasks.setIsChecked', this._id, !this.isChecked);
    },
    'click .delete'() {
        // TasksCollection.remove(this._id);
        Meteor.call('tasks.remove', this._id);
    },
});